
import sys #default import, not actually being used by this program, could be used for debug
from reportlab.lib import colors #reportlab imports are for tables and table formatting
from reportlab.lib.pagesizes import letter, inch
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
from PyPDF2 import PdfFileWriter, PdfFileReader #PyPDF2 allows reading and writing of pdfs
from reportlab.platypus.flowables import Image #reportlab import which allows image handling
import json #JavaScript Object Notation import
from pprint import pprint #console printing import for debug
from reportlab.platypus import Indenter #allows indentation of elements

#getting data as JSON object from a file
with open('data2') as data_file:    
    data = json.load(data_file)


#pprint(data) # debug

#pprint(lognum) # debug


#PDF doc setup
doc = SimpleDocTemplate("simple_table_grid.pdf", pagesize=letter,  rightMargin=0,leftMargin=0,
                        topMargin=0,bottomMargin=0) #default doc object with no margins 

#container for the 'Flowable' objects, will hold the entire page, then append to the doc
elements = []


'''''''''''''''''''''''''''''
Header 
'''''''''''''''''''''''''''''

#adding the logo to the top of the page
headerlogo = Image("Form Header.jpg", 8.5*inch, 1.5*inch) #sizing the logo
elements.append(headerlogo) #appending logo to flowable


#creating the header table object with default labels, the table contains a middle column, the 4th column, as padding
HeaderData = [["Sale Name", ":", data['salename'],"", "Contract#", ":", data['contractnum'],"","Contract Minimums"],
              ["Logger", ":", data['logger'],"", "Source Load#", ":", data['sourceloadnum'],"","Diameter: " + str(data['contractmindia'])],
              ["Seller", ":", data['seller'],"", "Seller Load#", ":", data['sellerloadnum'],"","Length: " + str(data['contractminlen'])],
              ["Buyer", ":", data['buyer'],"", "Buyer Load#", ":", data['buyerloadnum'],"",""],
              ["Trucker", ":", data['trucker'],"", "Truck#", ";", data['trucknum'],"","Weight"],
              ["Destination", ":", data['destination'],"", "Scaler Code", ";", data['scalercode'],"","Gross: " + str(data['weightgross'])],
              ["Deck", ":", data['deck'],"", "Sale Hammer", ";", data['salehammer'],"","Tare: " + str(data['weighttare'])],
              ["Scale Type", ":", data['scaletype'],"", "", "", "","","Net: " + str(data['weightnet'])]  
              ]


#defining the header table object as a reportlab table
HeaderTable = Table(HeaderData,9*[0.7*inch],8*[0.2*inch], spaceBefore=12, spaceAfter=12, hAlign='LEFT')

#defining the style for the reportlab table object
HeaderTable.setStyle(TableStyle([('ALIGN',(0,0), (-1,-1),'LEFT'),
                        ('ALIGN',(0,0), (0,7),'RIGHT'), #aligns the first column to the right
                        ('ALIGN',(4,0), (4,7),'RIGHT'),  #aligns the fourth column to the right
                        ('ALIGN',(8,0), (8,7),'LEFT'),        
                        ('SIZE',(0,0), (-1,-1),9.5),
                        ('VALIGN',(0,0), (-1,-1),'BOTTOM'),
                       ]))

#appending the table to the final flowable obj
elements.append(Indenter(left=0.7*inch)) #adds indentation
elements.append(HeaderTable)
elements.append(Indenter(left=-0.7*inch)) #removes indentation for future elements

'''''''''''''''''''''''''''''
Main Table
'''''''''''''''''''''''''''''



#getting data from the JSON obj
TableData = data['logs']
lognum = len(data['logs'])

#Inserting the 2nd to top row of data, the labels
TableData.insert(0, [
      "Log#", "LG","D1","D2","SP","SSL","DL","DD","%D","FT","GD","DL","DD","%D","FT",
      "GD","DL", "DD", "%D", "FT", "GD", "MSM", "GRS", "NET", "SSL", "UC", "SC"
      ])

#Inserting the flying row with segment labels
TableData.insert(0, [
      "",  "", "", "", "","","", "",
      "SEGMENT ONE",
      "","","","",
      "SEGMENT TWO",
      "", "", "", "",
      "SEGMENT THREE",
      "","","","","","","",""
      ])

lognum = lognum + 2
#pprint(TableData) #debug

#pprint(data['logs']) #debug

#sizing the main data table       
MainTable=Table(TableData,26*[0.3*inch], lognum*[0.25*inch], spaceBefore=12, spaceAfter=12, hAlign='CENTER')

#making the dummy data table look pretty
MainTable.setStyle(TableStyle([('ALIGN',(0,0), (-1,-1),'LEFT'), #aligns all cells to the left
                        ('ALIGN',(0,0), (25,0),'CENTER'), #aligns top row as center
						            ('SIZE',(0,0), (-1,-1),6.5), #sets the text size for the table
                        ('VALIGN',(0,0), (-1,-1),'BOTTOM'),
                        ('INNERGRID', (0,1), (-1,-1), 0.25, colors.lightgrey),
                        ('BOX', (0,1), (-1,-1), 0.25, colors.black),
                        ]))

#add Main Table to the flowable obj
elements.append(MainTable)

'''''''''''''''''''''''''''''
Summary Table
'''''''''''''''''''''''''''''

#getting the summary table info from the JSON obj
data2 = data['tots']
totnum = len(data['tots'])
 
SummaryTable=Table(data2,8*[0.4*inch], totnum*[0.25*inch], spaceBefore=24, spaceAfter=12, hAlign='LEFT')

SummaryTable.setStyle(TableStyle([('ALIGN',(0,0), (-1,-1),'LEFT'),
            ('SIZE',(0,0), (-1,-1),6.5),
                       ('VALIGN',(0,0), (-1,-1),'BOTTOM'),
                       ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                       ('BOX', (0,0), (-1,-1), 0.25, colors.black),
                       ]))

#adding the summary table to the flowable object
elements.append(Indenter(left=0.2*inch)) #adds indentation
elements.append(SummaryTable)
elements.append(Indenter(left=-0.2*inch)) #removes indentatino for future objects


# write the document to file
doc.build(elements)