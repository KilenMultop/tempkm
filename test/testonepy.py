#!/usr/bin/python

print("hello world, it's me")

import http.server
import socketserver

PORT = 8000

Handler = http.server.SimpleHTTPRequestHandler
httpd = socketserver.TCPServer(("",PORT), Handler)

print("Serving at Port:" + PORT)

httpd.serve_forever()